var ms, sec, min, hr, day, mon, year, mon_z;
module.exports = {
    get: function (type, mode) {
        var time = new Date();
        if (mode != 'now') { time.setTime(mode) }
        ms = time.getMilliseconds(); sec = time.getSeconds();
        min = time.getMinutes(); hr = time.getHours();
        day = time.getDate(); mon = time.getMonth() + 1; year = time.getFullYear();
        if (mon < 10) { mon_z = "0" } else { mon_z = "" }
        if (ms >= 100) { ms = (ms - (ms % 10)) / 10 }
        if (ms < 10) { ms = '0' + ms }
        if (hr < 10) { hr = '0' + hr }
        if (min < 10) { min = '0' + min }
        if (sec < 10) { sec = '0' + sec }
        switch (type) {
            case 'all':
                return year + "/" + mon_z + mon + "/" + day + " " + hr + ":" + min + ":" + sec + "." + ms; break;
            case 'all_no_ms':
                return year + "/" + mon_z + mon + "/" + day + " " + hr + ":" + min + ":" + sec; break;
            case 'for_filename':
                return year + "-" + mon_z + mon + "-" + day + " " + hr + min + sec + "." + ms;
                break;
            case 'for_log':
                return year + "-" + mon_z + mon + "-" + day + " " + hr + min + sec;
                break;
            case 'hmsms':
                return hr + ":" + min + ":" + sec + "." + ms;
            case 'ms': return ms; break;
            case 'sec': return sec; break;
            case 'min': return min; break;
            case 'hr': return hr; break;
            case 'day': return day; break;
            case 'mon': return mon; break;
            case 'year': return year; break;
            case 'tp': return time.getTime(); break;
            case 'json':
                let time_json = { "year": year, "month": mon, "day": day, "hr": hr, "min": min, "sec": sec, "ms": ms };
                return time_json; break;
        }
    }
}