//-------------- Setting
const key = require("../data/key.json");

//-------------- API
const LineBotSDK = require("@line/bot-sdk");
const LineBotClient = new LineBotSDK.Client(key.Bot);
const fs = require("fs");
const sqlite3 = require("sqlite3");

//-------------- My Function
const time = require("./time.js");
const log = require("./log.js");

//-------------- Module Variable
var profile_list = [];
var user_list = [];
var group_list = [];
var db_profile;
var db_user;
var db_group;

module.exports = {
    // local儲存檔案
    save_media: function save_media(msg_id, timestamp, name, type) {
        // 以msg_id連接getMessageContent 取得檔案
        // stream.on data 持續下載檔案
        // stream.on end 下載完成後 進行提醒
        LineBotClient.getMessageContent(msg_id).then(stream => {
            var file_extension = stream.headers['content-type'].split('/')[1]
            if (type == "audio") { file_extension = "m4a" }
            var file_time = time.get("for_filename", timestamp)
            var file = fs.createWriteStream("./data/file/" + type + "/" + file_time + " " + name + "." + file_extension)
            var type_str = ""
            switch (type) {
                case "image": type_str = "圖片"; break;
                case "audio": type_str = "聲音"; break;
                case "video": type_str = "影片"; break;
            }
            stream.on('data', chunk => { file.write(chunk) });
            stream.on('end', () => {
                file.end();
                log.file(type_str + "儲存完成 : " + file_time + " " + name + "." + file_extension)
            });
        });
    },
    // 同上 為file檔案類型處理
    save_file: function save_file(msg_id, user_name, file_name) {
        LineBotClient.getMessageContent(msg_id).then(stream => {
            var file = fs.createWriteStream("./data/file/file/[" + user_name + "] " + file_name)
            stream.on('data', chunk => { file.write(chunk) });
            stream.on('end', () => {
                file.end();
                log.file("檔案儲存完成 : file/[" + user_name + "] " + file_name)
            });
        });
    },


    // 資料庫index初始化
    profile_db_init: function profile_db_init() {
        return new Promise((resolve) => {
            db_profile = new sqlite3.Database("./data/database/profile.sqlite")
            profile_list = []; var empty = true;
            db_profile.each("select id from profile;", function (err, value) {
                if (err) throw err; empty = false;
                profile_list.push(value.id);
                resolve();
            })
            if (empty == true) { resolve() }
        })
    },
    group_db_init: function group_db_init() {
        return new Promise((resolve) => {
            db_group = new sqlite3.Database("./data/database/group.sqlite")
            group_list = []; var empty = true;
            db_group.each("select id from group_index;", function (err, value) {
                if (err) throw err; empty = false;
                group_list.push(value.id)
                resolve();
            })
            if (empty == true) { resolve() }
        })
    },

    // 如果profile沒有這個人的資料 就記錄進去
    profile_record: function profile_record_test(id, name) {
        return new Promise(function (resolve) {
            if (profile_list.indexOf(id) == -1) {
                var URIname = encodeURIComponent(name)
                db_profile.run('insert into profile values ("' + id + '","' + URIname + '","' + URIname + '","");'
                    , function (err) {
                        if (err) throw err;
                        profile_list.push(id);
                        log.info("profile 添加新使用者: " + name + "  > " + id)
                        resolve();
                    })
            } else { resolve(); }
        })
    },


    // 記錄群組訊息 database
    // 如果index內沒有這群組:先記錄group id至index 再建立此群組的資料表 最後存訊息
    // 若有 僅存訊息即可
    group_chat_record: function group_chat_record(gid, uid, name, time, type, text) {
        return new Promise(function (resolve) {
            if (group_list.indexOf(gid) == -1) {
                db_group.serialize(function () {
                    db_group.run("insert into group_index values ('" + gid + "');",
                        function (err) { if (err) throw err; group_list.push(gid); });
                    db_group.run("create table '" + gid + "' ('time' integer,'uid' text,'name' text,'type' text,'text' text);",
                        function (err) { if (err) throw err; });
                    var URIname = encodeURIComponent(name); var URItext = encodeURIComponent(text);
                    db_group.run('insert into ' + gid + ' values (' + time + ',"' + uid + '","' + URIname + '","' + type + '","' + URItext + '");',
                        function (err) { if (err) throw err; });
                    resolve();
                })
            } else {
                db_group.serialize(function () {
                    var URIname = encodeURIComponent(name); var URItext = encodeURIComponent(text);
                    db_group.run('insert into ' + gid + ' values (' + time + ',"' + uid + '","' + URIname + '","' + type + '","' + URItext + '");',
                        function (err) { if (err) throw err; resolve() });
                })
            }
        })
    },
}