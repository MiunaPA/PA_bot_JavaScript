//-------------- Setting
const key = require("../data/key.json");
const config = require("../data/config.json");

//-------------- API
const LineBotSDK = require("@line/bot-sdk");
const LineBotClient = new LineBotSDK.Client(key.Bot);

//-------------- My Function
const record = require("./record.js");
const log = require("./log.js");


module.exports = {
    // 當收到任何post時
    post: async function (event) {
        switch (event.type) {
            case "message":
                module.exports.get_profile(event)
                break;
        }
    },


    get_profile: async function (event) {
        // 取得profile
        if (event.source.userId != undefined) {
            switch (event.source.type) {
                case "user":
                    var user_profile = await module.exports.get_profile_user(event.source.userId);
                    break;
                case "group":
                    var user_profile = await module.exports.get_profile_group(event.source.groupId, event.source.userId);
                    break;
                case "room":
                    var user_profile = await module.exports.get_profile_room(event.source.roomId, event.source.userId);
                    break;
            }
            var displayName = user_profile.displayName
            // 記錄profile
            record.profile_record(event.source.userId, displayName)
            // 處理訊息
            module.exports.msg_process(event, displayName);
        } else {
            // 抓不到id就不紀錄profile了 
            var displayName = "沒有ID"; event.source.userId = "No_userId"
            module.exports.msg_process(event, displayName);
        }
    },


    // 將收到的訊息進行處理
    msg_process: async function (event, displayName) {
        // 訊息格式化及處理
        var msg = {}; msg.name = displayName;
        switch (event.message.type) {
            case "text":
                msg.type = "";
                msg.str = event.message.text;
                break;
            case "image":
                msg.type = "(圖片) "; msg.str = "id:" + event.message.id;
                if (config.set.save_local.image == true) {
                    record.save_media(event.message.id, event.timestamp, msg.name, 'image');
                }
                break;
            case "video":
                msg.type = "(影片) "; msg.str = "id:" + event.message.id;
                if (config.set.save_local.video == true) {
                    record.save_media(event.message.id, event.timestamp, msg.name, 'video');
                }
                break;
            case "audio":
                msg.type = "(聲音) "; msg.str = "id:" + event.message.id;
                if (config.set.save_local.audio == true) {
                    record.save_media(event.message.id, event.timestamp, msg.name, 'audio');
                }
                break;
            case "file":
                msg.type = "(檔案) ";
                msg.str = "id:" + event.message.id +
                    " ; fileName:" + event.message.fileName +
                    " ; fileSize:" + event.message.fileSize;
                if (config.set.save_local.file == true) {
                    record.save_file(event.message.id, msg.name, event.message.fileName);
                }
                break;
            case "location":
                msg.type = "(位置) ";
                if (event.message.title == undefined) { event.message.title = "NoTitle" }
                msg.str = "id:" + event.message.id +
                    " ; title:" + event.message.title +
                    " ; address:" + event.message.address +
                    " ; latitude:" + event.message.latitude +
                    " ; longitude:" + event.message.longitude;
                break;
            case "sticker":
                msg.type = "(貼圖) ";
                msg.str = "stickerId:" + event.message.stickerId + " ; packageId:" + event.message.packageId;
                break;
        }
        // 處理完訊息 將訊息記錄至database
        switch (event.source.type) {
            case "user":
                module.exports.user_log(event.source.userId, msg.name, event.timestamp, event.message.type, msg.str).
                    then(() => { log.res(msg.str, "U", msg.name, msg.type) }); break;
            case "group":
                module.exports.group_log(event.source.groupId, event.source.userId, msg.name, event.timestamp, event.message.type, msg.str).
                    then(() => { log.res(msg.str, "G", msg.name, msg.type) }); break;
        }
    },


    // message記錄到database
    user_log: function (uid, name, time, type, text) {
        return new Promise(function (resolve) {
            if (config.set.chat_to_db == true) {
                record.user_chat_record(uid, name, time, type, text).then(() => resolve())
            } else { resolve() }
        })
    },
    group_log: function (gid, uid, name, time, type, text) {
        return new Promise(function (resolve) {
            if (config.set.chat_to_db == true) {
                record.group_chat_record(gid, uid, name, time, type, text).then(() => resolve())
            } else { resolve() }
        })
    },


    // 取得profile
    get_profile_user: async function (id) {
        var user_profile = await LineBotClient.getProfile(id);
        return user_profile;
    },
    get_profile_group: async function (id, group_id) {
        var user_profile = await LineBotClient.getGroupMemberProfile(id, group_id);
        return user_profile;
    },
    get_profile_room: async function (id, room_id) {
        var user_profile = await LineBotClient.getRoomMemberProfile(id, room_id);
        return user_profile;
    }
}
