//-------------- API
const fs = require("fs");
const config = require("../data/config.json");

//-------------- My Function
const time = require("./time.js");

//-------------- Module Variable
var log_path;

module.exports = {
    // 先偵測資料夾 > 再建立log檔
    init: function init() {
        return new Promise((resolve) => {
            module.exports.folder().
                then(() => module.exports.create()).
                then(() => { resolve() })
        })
    },


    // 如果log資料夾不存在就建立資料夾
    folder: function folder() {
        return new Promise((resolve) => {
            if (fs.existsSync("./data/log")) { resolve() }
            else {
                fs.mkdirSync("./data/log")
                console.log(module.exports.time_get() + "[Info]: 找不到/data/log 建立一個新的(本訊息不紀錄)")
                resolve();
            }
        })
    },
    // 建立log檔
    create: function create() {
        return new Promise((resolve) => {
            if (config.set.log_file == true) {
                var filename = time.get("for_log", "now") + ".log";
                log_path = "./data/log/" + filename
                var title = "/////  " + filename + "\n\n";
                fs.writeFileSync(log_path, title);
                module.exports.info("建立log : " + filename)
                resolve();
            } else {
                module.exports.info("log記錄檔已關閉");
                resolve()
            }
        })
    },


    // 下分五種log格式化使用
    res: function res(text, from, name, type) {
        var log_text = module.exports.time_get() + "[Res]: {" + from + "}" + name + " ==> " + type + text;
        console.log(log_text.replace(new RegExp("\n", "g"), " "));
        if (config.set.log.res == true) { module.exports.write(log_text); }
    },
    send: function send(text, name, type) {
        var log_text = module.exports.time_get() + "[Send]: " + name + " <== " + type + text;
        console.log(log_text.replace(new RegExp("\n", "g"), " "));
        if (config.set.log.send == true) { module.exports.write(log_text); }
    },
    debug: function debug(text) {
        var log_text = module.exports.time_get() + "[Debug]: " + text;
        console.log(log_text.replace(new RegExp("\n", "g"), " "));
        if (config.set.log.debug == true) { module.exports.write(log_text); }
    },
    info: function info(text) {
        var log_text = module.exports.time_get() + "[Info]: " + text;
        console.log(log_text.replace(new RegExp("\n", "g"), " "));
        if (config.set.log.info == true) { module.exports.write(log_text); }
    },
    file: function file(text) {
        var log_text = module.exports.time_get() + "[File]: " + text;
        console.log(log_text.replace(new RegExp("\n", "g"), " "));
        if (config.set.log.info == true) { module.exports.write(log_text); }
    },


    // log檔寫入
    write: function write(log_text) {
        if (config.set.log_file == true) {
            log_text = log_text + "\n"
            fs.appendFileSync(log_path, log_text, function (err) { if (err) throw err; })
        }
    },


    // 時間格式化
    time_get: function time_get() {
        return "[" + time.get("hmsms", "now") + "] "
    },
}