//-------------- Setting
const config = require("../data/config.json");
const key = require("../data/key.json");

//-------------- API
const fs = require("fs");
const LineBotSDK = require("@line/bot-sdk");
const LineBotClient = new LineBotSDK.Client(key.Bot);

//-------------- My Function
const time = require("./time.js");
const log = require("./log.js");
const db = require("./db_init.js");

module.exports = {
    // 初始化開始 : 依序偵測及初始化
    // 1.先初始化log log.init   2.再初始化db db.init   3.再初始化file file_folder_detect
    // 完成 start_success_notice
    start: function start() {
        log.init().
            then(() => db.init()).
            then(() => module.exports.file_folder_detect()).
            then(() => module.exports.start_success_notice())
    },


    // local存檔 資料夾偵測 
    // 1.偵測data/file  2.再偵測下四項子資料夾  3.完成
    file_folder_detect: function file_folder_detect() {
        return new Promise((resolve) => {
            module.exports.create_file_root().then(() =>
                Promise.all([
                    module.exports.create_file(),
                    module.exports.create_image(),
                    module.exports.create_video(),
                    module.exports.create_audio()
                ])
            ).then(() => resolve())
        })
    },
    // 先偵測是否有data/file資料夾 沒有的話就建立一個新的
    create_file_root: function create_file_root() {
        return new Promise((resolve) => {
            if (fs.existsSync("./data/file")) { resolve() }
            else {
                fs.mkdirSync("./data/file");
                log.info("找不到/data/file 建立一個新的");
                resolve();
            }
        })
    },
    // 下四項偵測 file,image,audio,video
    create_file: function create_file() {
        return new Promise((resolve) => {
            if (fs.existsSync("./data/file/file")) { resolve() }
            else {
                fs.mkdirSync("./data/file/file");
                log.info("找不到/data/file/file 建立一個新的");
                resolve();
            }
        })
    },
    create_image: function create_image() {
        return new Promise((resolve) => {
            if (fs.existsSync("./data/file/image")) { resolve() }
            else {
                fs.mkdirSync("./data/file/image");
                log.info("找不到/data/file/image 建立一個新的");
                resolve();
            }
        })
    },
    create_audio: function create_audio() {
        return new Promise((resolve) => {
            if (fs.existsSync("./data/file/audio")) { resolve() }
            else {
                fs.mkdirSync("./data/file/audio");
                log.info("找不到/data/file/audio 建立一個新的");
                resolve();
            }
        })
    },
    create_video: function create_video() {
        return new Promise((resolve) => {
            if (fs.existsSync("./data/file/video")) { resolve() }
            else {
                fs.mkdirSync("./data/file/video");
                log.info("找不到/data/file/video 建立一個新的");
                resolve();
            }
        })
    },


    //全部初始化完成也沒問題了 開機完成 發送提醒
    start_success_notice: function start_notice() {
        // 開機Line提醒  >  log紀錄
        if (config.set.start_notice == true) {
            for (i = 0; i < config.start_notice_id.length; i++) {
                var start_text = time.get("all", "now") + " 開機完成";
                LineBotClient.pushMessage(config.start_notice_id[i], { type: "text", text: start_text });
                log.send(start_text, config.start_notice_id[i], "")
            }
        }
        log.info("Miuna Shiodome    ***** 初始化完成 *****")
    }
}


