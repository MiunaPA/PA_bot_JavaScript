//-------------- API
const fs = require("fs");
const sqlite3 = require("sqlite3");

//-------------- My Function
const log = require("./log.js");
const record = require("./record.js");

//-------------- Module Variable
var profile;
var user;
var group;

module.exports = {
    // 資料庫初始化
    // 資料夾 folder_detect (如果資料夾不存在就建立)) > 資料庫檔 file_detect (如果資料不存在就建立) > 讀取資料庫檔 load_db > 
    // 讀取table (如果table不存在就create table) table > 資料庫初始化完成,移至record內初始化 record_init > 
    // 全部完成後 回傳完成了 resolve()
    init: function start() {
        return new Promise((resolve) => {
            module.exports.folder_detect().
                then(() => module.exports.file_detect()).
                then(() => module.exports.load_db()).
                then(() => module.exports.table()).
                then(() => module.exports.record_init()).
                then(() => resolve())
        })
    },


    // 資料庫 資料夾偵測 > 不存在就建立 > 存在後 才能偵測裡面有沒有資料庫
    folder_detect: function db_folder_detec() {
        return new Promise((resolve) => {
            if (fs.existsSync("./data/database")) { resolve(); }
            else {
                fs.mkdirSync("./data/database");
                log.info("找不到/data/database 建立一個新的");
                resolve();
            }
        })
    },
    // 資料庫檔偵測 > 如果檔案不存在 建立一個空白檔案
    file_detect: function () {
        return new Promise((resolve) => {
            if (!fs.existsSync("./data/database/profile.sqlite")) { fs.writeFileSync("./data/database/profile.sqlite", ""); }
            if (!fs.existsSync("./data/database/group.sqlite")) { fs.writeFileSync("./data/database/group.sqlite", ""); }
            resolve()
        })
    },
    // 將資料庫檔讀取至變數 
    load_db: function () {
        return new Promise((resolve) => {
            profile = new sqlite3.Database("./data/database/profile.sqlite")
            group = new sqlite3.Database("./data/database/group.sqlite")
            resolve();
        })
    },
    // 資料庫偵測table是否存在 跑create table if not exists(如果表不存在時 create)
    table: function () {
        return new Promise((resolve) => {
            var profile_t = new Promise(function (resolve) {
                profile.run("create table if not exists 'profile' ('id' text,'name' text,'nick' text,'remark' text);",
                    function (err) { if (err) throw err; resolve() })
            })
            var group_t = new Promise(function (resolve) {
                group.run("create table if not exists 'group_index' ('id' text);",
                    function (err) { if (err) throw err; resolve() })
            })
            Promise.all([profile_t, group_t]).then(() => resolve());
        })
    },
    // 這裡的資料庫初始化完成了 將record db初始化
    record_init: function () {
        return new Promise((resolve) => {
            Promise.all([
                record.profile_db_init(),
                record.group_db_init()
            ]).then(() => { resolve() })
        })
    }
}