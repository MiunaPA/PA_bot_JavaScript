//-------------- API
const fs = require("fs");
const json = require("jsonfile");

module.exports = {
    data: function data() {
        return new Promise((resolve) => {
            var init_key = {
                Bot: {
                    channelSecret: "請將channelSecret填在這裡",
                    channelAccessToken: "請將channelAccessToken填在這裡"
                }
            }
            var init_config = {
                admin_user_id: [],
                start_notice_id: [],
                annotation: [
                    "admin_user_id 是管理員id 請最少填寫一位才能在LINE內使用管理員指令",
                    "start_notice 是開機提醒名單 當開機時會傳送提醒給指定id使用者"
                ],
                set: {
                    log_file: true,
                    log: {
                        res: true,
                        send: true,
                        debug: true,
                        info: true,
                        file: true
                    },
                    start_notice: false,
                    save_local: {
                        audio: true,
                        image: true,
                        video: true,
                        file: true
                    },
                    image_upload: false,
                    chat_to_db: true
                },
                port: 5000
            }
            if (!fs.existsSync("./data")) {
                fs.mkdirSync("./data");
                json.writeFileSync("./data/key.json", init_key);
                console.log(" 由於沒有 data 也沒有 key 已新建一個 key.json 在 data 資料夾內")
                console.log(" 請將 channelSecret , channelAccessToken 填寫在key.json內")
                console.log(" 填寫完後 再重新開啟程序\n\n 即將退出程序...\n")
                process.exit()
            } else {
                if (!fs.existsSync("./data/key.json")) {
                    json.writeFileSync("./data/key.json", init_key);
                    console.log(" 由於沒有 key 已新建一個 key.json 在 data 資料夾內")
                    console.log(" 請將 channelSecret , channelAccessToken 填寫在key.json內")
                    console.log(" 填寫完後 再重新開啟程序\n\n 即將退出程序...\n")
                    process.exit()
                }
                if (!fs.existsSync("./data/config.json")) {
                    json.writeFileSync("./data/config.json", init_config);
                    console.log(" 找不到config.json 已新建了一個在data內")
                }
            }
        })
    }
}