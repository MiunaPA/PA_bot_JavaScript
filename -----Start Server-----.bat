@echo off
chcp 65001
title PA Bot - Line Messaging API Bot 

echo.
echo  CMD Starting......
echo.

:Start
git fetch --all
git reset --hard origin/master

node app.js

echo.
echo   The program is over and will be restarted......
timeout /t 5
echo.

goto Start