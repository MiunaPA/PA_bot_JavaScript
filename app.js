//
// Miuna Shiodome
// Line Messaging API Bot
// Author : Miuna_PA
// Source Code : https://gitlab.com/MiunaPA/PA_bot_JavaScript
// 
//



//-------------- Detect Data Folder
const detect_data = require("./process/create_data");
detect_data.data();

//-------------- Setting
const key = require("./data/key.json");
const config = require("./data/config.json");

//-------------- API
const LineBotSDK = require("@line/bot-sdk");
const Koa = require("koa");
const KoaRouter = require("koa-router");
const KoaBodyParser = require("koa-bodyparser");
const app = new Koa();
const router = new KoaRouter();

//-------------- My Function
const receive_message = require("./process/receive_message.js");
const init = require("./process/init.js");


// Webhook
router.post("", ctx => {
    const req = ctx.request;
    if (LineBotSDK.validateSignature(req.rawBody, key.Bot.channelSecret, req.headers["x-line-signature"])) {
        ctx.status = 200; req.body.events.map(MessageHandler);
    } else { ctx.body = "驗證失敗"; ctx.status = 401; }
});
// Webhook


// 伺服器啟動
app.use(KoaBodyParser());
app.use(router.routes());
console.log("\n Server is starting...   Port:" + config.port + "\n")
app.listen(config.port);
// 伺服器啟動


// 收到訊息的處理程序
async function MessageHandler(Event) { receive_message.post(Event); }
// 收到訊息的處理程序


// 開機時要做的初始化程序
init.start()
